#-------------------------------------------------------------------------------
# Name:Taller 2 simulacion1
# Purpose:
#
# Author:      Stiveen Correa 1556134
#              Alvaro Javier Quintero 1556009
#              Kevin Santiago Lemos 1556
#
# Created:     21/08/2019
# Copyright:   (c) Stiveen13 and JF 2019
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#LIBRERIAS
import random
import simpy
import numpy
#Datos de la simulación
SEMILLA = random.seed(0) #Semilla generador
LLEGADA_CLIENTES =random.expovariate(1.0/4.0) #Distribucion exponencial de 4
COLA = 0
MAX_COLA = 0
ESPERA_CLIENTES = numpy.array([])
UTILIDAD = 0

#FUNCION RANDOM PROBABILIDAD PONDERADA
opciones = [('arepa con todo', 35), ('no ordeno', 15), ('arepa para el bebe', 25), ('arepa con morcilla', 25)]
prize_list = [prize for prize, weight in opciones for i in range(weight)]

#fUNCION LLEGADA
def llegada(env, contador):
    i = 0
    while True:
        i= i+1
        c = cliente(env, 'Cliente %02d' % i, contador)
        env.process(c)
        tiempo_llegada =random.expovariate(1.0/4.0)
        yield env.timeout(tiempo_llegada) #Yield retorna un objeto iterable
        if(env.now>480):
            break


#FUNCION CLIENTE
def cliente(env, nombre, servidor):
    #El cliente llega y se va cuando es atendido
    llegada = env.now
    print('%7.2f'%(env.now)," Llega el cliente ", nombre)
    global COLA
    global MAX_COLA
    global ESPERA_CLIENTES
    global UTILIDAD
    #Atendemos a los clientes (retorno del yield)
    #With ejecuta un iterador sin importar si hay excepciones o no
    with servidor.request() as req:

		#Hacemos la espera hasta que sea atendido el cliente
        COLA += 1
        if COLA > MAX_COLA:
            MAX_COLA = COLA

		#print("Tamaño cola", COLA)
        results = yield req
        COLA = COLA - 1
        espera = env.now - llegada
        ESPERA_CLIENTES = numpy.append(ESPERA_CLIENTES, espera)
        print('%7.2f'%(env.now), " El cliente ",nombre," espera a ser atendido ",espera)

        opcion_cliente = random.choice(prize_list)
        tiempo_atencion= 0

        if(opcion_cliente=='arepa con todo' ):
             tiempo_atencion=random.uniform(5,10)
             UTILIDAD += 750



        if(opcion_cliente=='no ordeno' ):
            tiempo_atencion=random.expovariate(1.0/1.5)


        if(opcion_cliente=='arepa para el bebe' ):
            tiempo_atencion=random.uniform(3,7)
            UTILIDAD += 550

        if(opcion_cliente=='arepa con morcilla' ):
            tiempo_atencion=random.expovariate(1.0/6.0)
            UTILIDAD += 500

        print('%7.2f'%(env.now), " El cliente ",nombre," tine la opcion ",opcion_cliente,"se demora",tiempo_atencion)

        yield env.timeout(tiempo_atencion)

        print('%7.2f'%(env.now), " Sale el cliente ",nombre)



#Inicio de la simulación

print('venta de arepas')
random.seed(SEMILLA)
env = simpy.Environment()

#Inicio del proceso y ejecución
#Empleado
servidor = simpy.Resource(env, capacity=1)
env.process(llegada(env, servidor))
env.run()

print("Cola máxima ",MAX_COLA)
print("Tiempo promedio de espera ",'%7.2f'%(numpy.mean(ESPERA_CLIENTES)))
print("La utilida de las ventas fue: ", UTILIDAD)